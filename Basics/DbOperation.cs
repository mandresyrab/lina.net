﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Lina.Net.Basics
{
    public abstract class DbOperation<T>
    {
        public void Controls(SqlConnection c){}
        public static LinkedList<PropertyInfo> GetWritableProperties()
        {
            Type type = typeof(T);
            PropertyInfo[] all = type.GetProperties();
            LinkedList<PropertyInfo> result = new LinkedList<PropertyInfo>();
            for (int i = 0; i < all.Length; i++)
            {
                if (all[i].CanWrite) result.AddLast(all[i]);
            }
            return result;
        }

        public static LinkedList<string> GetColumnNames(SqlConnection c)
        {
            string name = typeof(T).Name;
            return GetColumnNames(c, name);
        }

        public static LinkedList<string> GetColumnNames(SqlConnection c, string tableName)
        {
            string sql = $"SELECT * FROM {tableName}";
            SqlCommand cmd = new SqlCommand(sql, c);
            LinkedList<string> result = new LinkedList<string>();
            SqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                int nbCol = reader.FieldCount;
                for (int i = 0; i < nbCol; i++)
                {
                    result.AddLast(reader.GetName(i));
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null && !reader.IsClosed) reader.Close();
            }
        }

        public static LinkedList<PropertyInfo> GetDataBaseAssociatedField(SqlConnection c, string tableName)
        {
            LinkedList<PropertyInfo> properties = GetWritableProperties();
            LinkedList<string> columnNames = GetColumnNames(c, tableName);
            LinkedList<PropertyInfo> result = new LinkedList<PropertyInfo>();
            for (int i = 0; i < properties.Count; i++)
            {
                for (int j = 0; j < columnNames.Count; j++)
                {
                    if (properties.ElementAt(i).Name.ToUpper().Equals(columnNames.ElementAt(j).ToUpper()))
                    {
                        result.AddLast(properties.ElementAt(i));
                        columnNames.Remove(columnNames.ElementAt(j));
                        break;
                    }
                }
            }
            return result;
        }
        public void Delete(SqlConnection c)
        {
            string tablename = typeof(T).Name;
            Delete(c, tablename);
        }
        public void Delete(SqlConnection c, string tableName)
        {
            LinkedList<PropertyInfo> dataSet = GetDataBaseAssociatedField(c, tableName);
            string sql = $"DELETE FROM {tableName} WHERE ";
            string checkInit = sql;
            for (int i = 0; i < dataSet.Count; i++)
            {
                if (!sql.Equals(checkInit)) sql = $"{sql} AND";
                sql = $"{sql} {dataSet.ElementAt(i).Name}='{dataSet.ElementAt(i).GetValue(this).ToString()}' ";
            }
            SqlCommand cmd = new SqlCommand(sql, c);
            cmd.ExecuteNonQuery();
        }

        public void Update(SqlConnection c, string primaryKey)
        {
            string tableName = typeof(T).Name;
            Update(c, tableName, primaryKey);
        }

        public void Update(SqlConnection c, string tableName, string primaryKey)
        {
            Controls(c);
            if (primaryKey == null) throw new Exception("Primary Key should not be null");
            LinkedList<PropertyInfo> dataSet = GetDataBaseAssociatedField(c, tableName);
            string sql = $"UPDATE {tableName} SET ";
            string checkInit = sql;
            PropertyInfo primaryInfo = null;
            for (int i = 0; i < dataSet.Count; i++)
            {
                if (!dataSet.ElementAt(i).Name.ToUpper().Equals(primaryKey.ToUpper()))
                {
                    if (!sql.Equals(checkInit)) sql = $"{sql} ,";
                    sql = $"{sql} {dataSet.ElementAt(i).Name}='{dataSet.ElementAt(i).GetValue(this).ToString()}'";
                }
                else primaryInfo = dataSet.ElementAt(i);
            }
            if (primaryInfo == null) throw new Exception("Primary Key Invalid");
            sql = $"{sql} WHERE {primaryInfo.Name} = '{primaryInfo.GetValue(this).ToString()}'";
            SqlCommand cmd = new SqlCommand(sql, c);
            cmd.ExecuteNonQuery();
        }

        public void Insert(SqlConnection c)
        {
            string tableName = typeof(T).Name;
            Insert(c, tableName);
        }

        public void Insert(SqlConnection c, string tableName)
        {
            Controls(c);
            LinkedList<PropertyInfo> dataSet = GetDataBaseAssociatedField(c, tableName);
            string sql = $"INSERT INTO {tableName}(";
            string checkInit = sql;
            for (int i = 0; i < dataSet.Count; i++)
            {
                if (!sql.Equals(checkInit)) sql = $"{sql} ,";
                sql = $"{sql} {dataSet.ElementAt(i).Name}";
            }
            sql = $"{sql} ) VALUES (";
            checkInit = sql;
            for (int i = 0; i < dataSet.Count; i++)
            {
                if (!sql.Equals(checkInit)) sql = $"{sql} ,";
                sql = $"{sql} '{dataSet.ElementAt(i).GetValue(this).ToString()}'";
            }
            sql = $"{sql} )";
            SqlCommand cmd = new SqlCommand(sql, c);
            cmd.ExecuteNonQuery();
        }

        public static LinkedList<T> Find(SqlConnection c)
        {
            string tableName = typeof(T).Name;
            return Find(c, tableName);
        }

        public static LinkedList<T> Find(SqlConnection c, string tableName)
        {
            Type type = typeof(T);
            LinkedList<PropertyInfo> dataSet = GetDataBaseAssociatedField(c, tableName);
            string sql = "SELECT ";
            string checkInit = sql;
            for (int i = 0; i < dataSet.Count; i++)
            {
                if (!sql.Equals(checkInit)) sql = $"{sql} ,";
                sql = $"{sql} {dataSet.ElementAt(i).Name}";
            }
            sql = $"{sql} FROM {tableName}";
            SqlCommand cmd = new SqlCommand(sql, c);
            SqlDataReader reader = null;
            try
            {
                reader = cmd.ExecuteReader();
                LinkedList<T> result = new LinkedList<T>();
                while (reader.Read())
                {
                    T obj = (T)Activator.CreateInstance(type);
                    for (int i = 0; i < dataSet.Count; i++)
                    {
                        if (reader.IsDBNull(i))
                        {
                            try
                            {
                                dataSet.ElementAt(i).SetValue(obj, null);
                            }
                            catch (Exception)
                            {
                                dataSet.ElementAt(i).SetValue(obj, 0);
                            }
                        }
                        else dataSet.ElementAt(i).SetValue(obj, reader.GetValue(i));
                    }
                    result.AddLast(obj);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null && !reader.IsClosed) reader.Close();
            }
        }
    }
}
